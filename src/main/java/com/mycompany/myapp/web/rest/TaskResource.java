package com.mycompany.myapp.web.rest;

import java.util.UUID;

import com.mycompany.myapp.Tarefa;
import com.mycompany.myapp.ProducerChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Task controller
 */
@RestController
@RequestMapping("/api/task")
public class TaskResource {

    private final Logger log = LoggerFactory.getLogger(TaskResource.class);

    private MessageChannel channel;

    public TaskResource(ProducerChannel channel) {
        this.channel = channel.messageChannel();
    }

    /**
    * POST add
    */
    @PostMapping("/add")
    public @ResponseBody Tarefa add(@RequestBody Tarefa tarefa) {
        String uniqueID = UUID.randomUUID().toString();

        //Tarefa result = new Tarefa(uniqueID, "Tarefa", 123, "Micael", "micael@mail.com", false, false);
        tarefa.setUniqid(uniqueID);
        tarefa.setAction("INSERT");
        channel.send(MessageBuilder.withPayload(tarefa).build());

        return tarefa;
    }

    @PostMapping("/reject")
    public @ResponseBody Tarefa reject(@RequestBody Tarefa tarefa) {
        tarefa.setRejeitada(true);
        tarefa.setAction("UPDATE");
        channel.send(MessageBuilder.withPayload(tarefa).build());

        return tarefa;
    }

    @PostMapping("/done")
    public @ResponseBody Tarefa done(@RequestBody Tarefa tarefa) {
        tarefa.setEfectuada(true);
        channel.send(MessageBuilder.withPayload(tarefa).build());

        return tarefa;
    }

    @PostMapping("/delete") // sned uniqid and not id
    public @ResponseBody Tarefa done(@RequestBody String id) {
        Tarefa tarefa = new Tarefa();
        tarefa.setAction("DELETE");
        tarefa.setUniqid(id);
        channel.send(MessageBuilder.withPayload(tarefa).build());

        return tarefa;
    }

}
