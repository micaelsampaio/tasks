package com.mycompany.myapp;

public class Tarefa {

    private String uniqid;
    private String descricao;
    private int idContacto;
    private String nomeContacto;
    private String emailContacto;
    private boolean rejeitada;
    private boolean efectuada;

    private String action = "";
    public Tarefa(){

    }
    public Tarefa(String uniqid, String descricao, int idContacto, String nomeContacto, String emailContacto,
            boolean rejeitada, boolean efectuada) {
        this.uniqid = uniqid;
        this.descricao = descricao;
        this.idContacto = idContacto;
        this.nomeContacto = nomeContacto;
        this.emailContacto = emailContacto;
        this.rejeitada = rejeitada;
        this.efectuada = efectuada;
    }

    public String getUniqid() {
        return this.uniqid;
    }
    public void setUniqid(String uniqid){
        this.uniqid = uniqid;
    }
    public String getDescricao() {
        return this.descricao;
    }

    public int getIdContacto() {
        return this.idContacto;
    }

    public String getNomeContacto() {
        return this.nomeContacto;
    }

    public String getemailContacto() {
        return this.emailContacto;
    }

    public boolean isRejeitada() {
        return this.rejeitada;
    }

    public boolean isefectuada() {
        return this.efectuada;
    }

    public void setRejeitada(boolean rejeitada) {
         this.rejeitada = rejeitada;
    }

    public void setEfectuada(boolean efectuada) {
         this.efectuada = efectuada;
    }

    public void setAction(String action){
        this.action = action;
    }
    public String getAction(String action){
        return action;
    }
}